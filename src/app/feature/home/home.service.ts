import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor() {}

  getHotels() {
    return [
      {
        id: '249942',
        name: 'Hotel Stefanos',
        stars: 3,
        price: 994.18,
        image:
          'https://images.almundo.com/205/image/fetch/fl_progressive,q_auto:eco,w_2000,f_jpg/https://static.almundo.com/img/destinations/city-BOG--large.jpg',
        amenities: [
          'safety-box',
          'nightclub',
          'deep-soaking-bathtub',
          'beach',
          'business-center'
        ]
      },
      {
        id: '249942',
        name: 'Hotel Stefanos',
        stars: 3,
        price: 994.18,
        image:
          'https://images.almundo.com/205/image/fetch/fl_progressive,q_auto:eco,w_2000,f_jpg/https://static.almundo.com/img/destinations/city-BOG--large.jpg',
        amenities: [
          'safety-box',
          'nightclub',
          'deep-soaking-bathtub',
          'beach',
          'business-center'
        ]
      }
    ];
  }
}
