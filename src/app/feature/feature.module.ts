import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { FeatureRoutingModule } from './feature-routing.module';
import { MainLayoutComponent } from '../core/main-layout/main-layout.component';
import { CardComponent } from '../shared/card/card.component';

@NgModule({
  declarations: [HomeComponent, MainLayoutComponent, CardComponent],
  imports: [CommonModule, FeatureRoutingModule],
  bootstrap: [HomeComponent]
})
export class FeatureModule {}
