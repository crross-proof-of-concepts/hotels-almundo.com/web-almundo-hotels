import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shared-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() hotel: any;
  stars: Array<number>;

  constructor() {}

  ngOnInit() {
    if (this.hotel.stars) {
      this.stars = new Array(this.hotel.stars);
    }
  }
}
